<?php

namespace ScalarTypes;

class Integer
{
    private $value;

    /**
     * @param int $value
     */
    public function __construct($value)
    {
        if(is_int($value)){
            $this->value = $value;
        }else{
            throw new ScalarException(get_class(), $value);
        }
    }

    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string)$this->value;
    }

    public function getMod($secondValue)
    {
        return $this->value % $secondValue;
    }

    public function toBinary()
    {
        return decbin($this->value);
    }

    public function toHexidecimal()
    {
        return dechex($this->value);
    }

    public function squareRoot()
    {
        return sqrt($this->value);
    }
}