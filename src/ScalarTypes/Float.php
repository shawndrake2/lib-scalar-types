<?php

namespace ScalarTypes;

class Float
{
    private $value;

    /**
     * @param float $value
     */
    public function __construct($value)
    {
        if (is_float($value)) {
            $this->value = $value;
        } else {
            throw new ScalarException(get_class(), $value);
        }
    }

    public function getValue()
    {
        return (float) $this->value;
    }

    public function ceil()
    {
        return ceil($this->value);
    }

    public function floor()
    {
        return floor($this->value);
    }

    public function round()
    {
        return round($this->value);
    }

    public function fmod($float)
    {
        return fmod($this->value, (float) $float);
    }

    public function format($decimals)
    {
        return number_format($this->value, (int) $decimals);
    }

    public function currency()
    {
        $format = $this->isNegative() ? '%(.2n' : '%.2n';

        // Returns US currency
        setlocale(LC_MONETARY, 'en_US.UTF-8');
        return money_format($format, $this->value);
    }

    public function __toString()
    {
        return (string) $this->value;
    }

    private function isNegative()
    {
        return (abs($this->value) != $this->value) ?: false;
    }
}