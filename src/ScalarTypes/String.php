<?php

namespace ScalarTypes;

class String
{
    private $value;

    /**
     * @param string $value
     */
    public function __construct($value)
    {
        if(is_string($value)){
            $this->value = $value;
        }else{
            throw new ScalarException(get_class(), $value);
        }
    }

    public function __toString()
    {
        return $this->value;
    }

    public function length()
    {
        return strlen($this->value);
    }

    public function toLower()
    {
        return strtolower($this->value);
    }

    public function toUpper()
    {
        return strtoupper($this->value);
    }

    public function rTrim($mask = null)
    {
        if ($mask) {
            return rtrim($this->value, $mask);
        } else {
            return rtrim($this->value);
        }
    }

    public function lTrim($mask = null)
    {
        if ($mask) {
            return ltrim($this->value, $mask);
        } else {
            return ltrim($this->value);
        }
    }

    public function trim($mask = null)
    {
        if ($mask) {
            return trim($this->value, $mask);
        } else {
            return trim($this->value);
        }
    }

    public function explode($delimiter = ".")
    {
        return explode($delimiter, $this->value);
    }

    public function htmlEntities()
    {
        return htmlentities($this->value);
    }

    public function decodeHtmlEntities()
    {
        return html_entity_decode($this->value);
    }

    public function stripTags($exceptions = null)
    {
        return strip_tags($this->value, $exceptions);
    }

    public function upperCaseFirst()
    {
        return ucfirst($this->value);
    }

    public function upperCaseWords()
    {
        return ucwords($this->value);
    }
}