<?php

namespace ScalarTypes;

class ScalarException extends \Exception
{
    const EXCEPTION_MESSAGE = "Value \"%s\" is not of type %s";

    public function __construct($class, $value)
    {
        $classname = end(explode('\\', $class));
        parent::__construct(sprintf(self::EXCEPTION_MESSAGE, $value, $classname));
    }
}