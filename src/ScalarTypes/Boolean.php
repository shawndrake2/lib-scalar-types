<?php

namespace ScalarTypes;

class Boolean
{
    private $value;

    /**
     * @param bool $value
     */
    public function __construct($value)
    {
        if (is_bool($value)) {
            $this->value = $value;
        } else {
            throw new ScalarException(get_class(), $value);
        }
    }

    // This is not ideal, but I haven't found an easier way to get the boolean value
    public function getValue()
    {
        return $this->value;
    }

    // Ideally, this would return the boolean value but will return the strings "true" or "false"
    public function __toString()
    {
        return var_export($this->value, true);
    }
}