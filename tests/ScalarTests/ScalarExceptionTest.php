<?php

namespace tests\ScalarTests;

use ScalarTypes\ScalarException;

class ScalarExceptionTest extends \PHPUnit_Framework_TestCase
{
    private $e;

    public function setUp()
    {
        $this->e = new ScalarException('\ScalarTypes\Integer', "foo");
    }

    public function testIsInstanceOfException()
    {
        $this->assertInstanceOf('\ScalarTypes\ScalarException', $this->e);
    }

    public function testCorrectErrorMessage()
    {
        $this->assertEquals('Value "foo" is not of type Integer', $this->e->getMessage());
    }
}