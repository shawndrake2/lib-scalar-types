<?php

namespace tests\ScalarTests;

use ScalarTypes\Integer;

class IntegerTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructsIfInt()
    {
        $value = new Integer(3);
        $this->assertInstanceOf('\ScalarTypes\Integer', $value);
    }

    public function testDoesNotConstructIfFloat()
    {
        try {
            $value = new Integer(3.14);
        } catch (\Exception $e) {
            $this->assertEquals('Value "3.14" is not of type Integer', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfBool()
    {
        try {
            $value = new Integer(true);
        } catch (\Exception $e) {
            $this->assertEquals('Value "1" is not of type Integer', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfString()
    {
        try {
            $value = new Integer("foo");
        } catch (\Exception $e) {
            $this->assertEquals('Value "foo" is not of type Integer', $e->getMessage());
        }
    }

    public function testToStringEchoesValue()
    {
        $value = new Integer(3);
        $this->assertEquals('3', $value);
    }

    public function testGetMod()
    {
        $value = new Integer(3);
        $this->assertEquals(3, $value->getMod(4));
        $value = new Integer(4);
        $this->assertEquals(1, $value->getMod(3));
    }

    public function testGetValue()
    {
        $value = new Integer(3);
        $this->assertEquals(3, $value->getValue());
    }

    public function testToBinary()
    {
        $value = new Integer(3);
        $this->assertEquals(11, $value->toBinary());
        $value = new Integer(156);
        $this->assertEquals(10011100, $value->toBinary());
    }

    public function testToHexidecimal()
    {
        $value = new Integer(3);
        $this->assertEquals("3", $value->toHexidecimal());
        $value = new Integer(156);
        $this->assertEquals("9c", $value->toHexidecimal());
        $value = new Integer(156430);
        $this->assertEquals("2630e", $value->toHexidecimal());
        $value = new Integer(-1);
        $this->assertEquals("ffffffffffffffff", $value->toHexidecimal());
    }

    public function testSquareRoot()
    {
        $value = new Integer(9);
        $this->assertEquals(3, $value->squareRoot());
        $value = new Integer(10);
        $this->assertEquals(3.1622776601683795, $value->squareRoot());
    }
}