<?php

namespace tests\ScalarTests;

use ScalarTypes\String;

class StringTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructsIfString()
    {
        $value = new String("foo");
        $this->assertInstanceOf('\ScalarTypes\String', $value);
    }

    public function testDoesNotConstructIfInt()
    {
        try {
            $value = new String(3);
        } catch (\Exception $e) {
            $this->assertEquals('Value "3" is not of type String', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfFloat()
    {
        try {
            $value = new String(3.14);
        } catch (\Exception $e) {
            $this->assertEquals('Value "3.14" is not of type String', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfBool()
    {
        try {
            $value = new String(true);
        } catch (\Exception $e) {
            $this->assertEquals('Value "1" is not of type String', $e->getMessage());
        }
    }

    public function testToStringEchoesValue()
    {
        $value = new String("foo");
        $this->assertEquals('foo', $value);
    }

    public function testLength()
    {
        $value = new String("foobar");
        $this->assertEquals(6, $value->length());
    }

    public function testToLower()
    {
        $value = new String("FOOBAR");
        $this->assertEquals("foobar", $value->toLower());
        $value = new String("foobar");
        $this->assertEquals("foobar", $value->toLower());
    }

    public function testToUpper()
    {
        $value = new String("FOOBAR");
        $this->assertEquals("FOOBAR", $value->toUpper());
        $value = new String("foobar");
        $this->assertEquals("FOOBAR", $value->toUpper());
    }

    public function testRtrim()
    {
        $value = new String("foo bar ");
        $this->assertEquals('foo bar', $value->rTrim());
        $this->assertEquals('foo b', $value->rTrim('r a'));
    }

    public function testLtrim()
    {
        $value = new String(" foo bar");
        $this->assertEquals('foo bar', $value->lTrim());
        $this->assertEquals('bar', $value->lTrim('f o'));
    }

    public function testTrim()
    {
        $value = new String(" foo bar ");
        $this->assertEquals('foo bar', $value->trim());
        $this->assertEquals('bar', $value->trim('f oa'));
    }

    public function testExplode()
    {
        $value = new String("foo.bar");
        $this->assertEquals(array("foo", "bar"), $value->explode());

        $value = new String("foo.bar boo.boo");
        $this->assertEquals(array("foo.bar", "boo.boo"), $value->explode(" "));
    }

    public function testHtmlEntities()
    {
        $value = new String("I'll \"walk\" the <b>dog</b> now");
        $this->assertEquals(
            "I'll &quot;walk&quot; the &lt;b&gt;dog&lt;/b&gt; now",
            $value->htmlEntities()
        );
    }

    public function testDecodeHtmlEntities()
    {
        $value = new String("I'll &quot;walk&quot; the &lt;b&gt;dog&lt;/b&gt; now");
        $this->assertEquals(
            "I'll \"walk\" the <b>dog</b> now",
            $value->decodeHtmlEntities()
        );
    }

    public function testStripTags()
    {
        $value = new String("<p>Test paragraph.</p><!-- Comment --> <a href=\"#fragment\">Other text</a>");
        $this->assertEquals(
            "Test paragraph. Other text",
            $value->stripTags()
        );
        $this->assertEquals(
            "<p>Test paragraph.</p> <a href=\"#fragment\">Other text</a>",
            $value->stripTags("<p></p><a></a>")
        );
    }

    public function testUpperCaseFirst()
    {
        $value = new String("foo bar");
        $this->assertEquals("Foo bar", $value->upperCaseFirst());
    }

    public function testUpperCaseWords()
    {
        $value = new String("foo bar");
        $this->assertEquals("Foo Bar", $value->upperCaseWords());
    }
}