<?php

namespace tests\ScalarTests;

use ScalarTypes\Boolean;

class BooleanTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructsIfBool()
    {
        $value = new Boolean(true);
        $this->assertInstanceOf('\ScalarTypes\Boolean', $value);
    }

    public function testDoesNotConstructIfInt()
    {
        try {
            $value = new Boolean(3);
        } catch (\Exception $e) {
            $this->assertEquals('Value "3" is not of type Boolean', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfFloat()
    {
        try {
            $value = new Boolean(3.14);
        } catch (\Exception $e) {
            $this->assertEquals('Value "3.14" is not of type Boolean', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfString()
    {
        try {
            $value = new Boolean("foo");
        } catch (\Exception $e) {
            $this->assertEquals('Value "foo" is not of type Boolean', $e->getMessage());
        }
    }

    public function testToStringEchoesValue()
    {
        $value = new Boolean(true);
        $this->assertEquals('true', $value);
    }

    public function testGetValueReturnsBooleanValue()
    {
        $value = new Boolean(true);
        $this->assertTrue($value->getValue());
        $value = new Boolean(false);
        $this->assertFalse($value->getValue());
    }
}