<?php

namespace tests\ScalarTests;

use ScalarTypes\Float;

class FloatTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructsIfFloat()
    {
        $value = new Float(3.14);
        $this->assertInstanceOf('\ScalarTypes\Float', $value);
    }

    public function testDoesNotConstructIfInt()
    {
        try {
            $value = new Float(3);
        } catch (\Exception $e) {
            $this->assertEquals('Value "3" is not of type Float', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfBool()
    {
        try {
            $value = new Float(true);
        } catch (\Exception $e) {
            $this->assertEquals('Value "1" is not of type Float', $e->getMessage());
        }
    }

    public function testDoesNotConstructIfString()
    {
        try {
            $value = new Float("foo");
        } catch (\Exception $e) {
            $this->assertEquals('Value "foo" is not of type Float', $e->getMessage());
        }
    }

    public function testToStringEchoesValue()
    {
        $value = new Float(3.14);
        $this->assertEquals('3.14', $value);
    }

    public function testGetValue()
    {
        $value = new Float(3.14);
        $this->assertEquals(3.14, $value->getValue());
    }

    public function testCeiling()
    {
        $value = new Float(3.14);
        $this->assertEquals(4, $value->ceil());
    }

    public function testFloor()
    {
        $value = new Float(3.14);
        $this->assertEquals(3, $value->floor());
    }

    public function testRound()
    {
        $value = new Float(3.14);
        $this->assertEquals(3, $value->round());

        $value = new Float(3.94);
        $this->assertEquals(4, $value->round());
    }

    public function testFmod()
    {
        $value = new Float(3.14);
        $this->assertEquals(0.04, $value->fmod(3.1));
    }

    public function testFormat()
    {
        $value = new Float(3.14159265359);
        $this->assertEquals(3.1416, $value->format(4));
        $this->assertNotEquals(3.1415, $value->format(4));

        $this->assertEquals(3.141593, $value->format(6));
        $this->assertNotEquals(3.141592, $value->format(6));

        $this->assertEquals(3.14159265, $value->format(8));
        $this->assertEquals(3.14, $value->format(2));

        // make sure decimal values don't screw up the format
        $this->assertEquals(3.1416, $value->format(4.2));
    }

    public function testCurrency()
    {
        // Default 2 decimals
        $value = new Float(3.14);
        $this->assertEquals("$3.14", $value->currency());

        // Default 1 decimal
        $value = new Float(3.1);
        $this->assertEquals("$3.10", $value->currency());

        // Negative value
        $value = new Float(-3.1);
        $this->assertEquals("($3.10)", $value->currency());
    }
}