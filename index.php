<?php

require 'vendor/autoload.php';

use ScalarTypes\Integer;
use ScalarTypes\Float;
use ScalarTypes\Boolean;
use ScalarTypes\String;

try {
    $int = new Integer(-37);
    $float = new Float(3.14);
    $bool = new Boolean(true);
    $bool2 = new Boolean(false);
    $string = new String("This is a string");
}catch (\Exception $e) {
    echo $e->getMessage();
    die;
}
?>

<p>Int Value: <?= $int ?></p>
<p>Float Value: <?= $float ?></p>
<p>Boolean Value: <?= $bool ?></p>
<p>Other Boolean Value: <?= $bool2 ?></p>
<p>String Value: <?= $string ?></p>
